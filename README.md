### Git命令

操作：1自己创建远程仓库 2创建本地仓库 3本地仓库与远程仓库关联
    *步骤如下
    1.[git init 初始化]
    2.[git add .将项目添加到暂存区]
        *注意： . 前面有空格，代表添加所有文件。
         若添加单个文件输入：git add xxxx.xx（xxxx.xx为文件名）
    3.[git commit -m "注释内容"]
	***注意：git使用commit命令后显示Author identity unknown
		-先输入：$ git config --global user.name “你的名字”
		-再输入：$ git config --global user.email “你的邮箱地址”
    ***创建远程仓库
        a.添加SSH KEY;
        b.新建repositories
    4.[git remote add origin https://github.com/xxxxx/test.git 本地仓库和远程仓库连接]
    5.[git push -u origin main 将本地项目推送到远程仓库]
    具体流程：先创建空本地仓库--复制上传内容到文件夹--提交到缓存区--提交到本地仓库--最后提交到远程仓库

git push "仓库地址"
    *①省略origin 远程主机名，第一个master 本地分支名，第二个master远程分支名
    git push
    *②
    git push orgin master
本地关联
查看仓库状态

### View Binding

*设置方式在Gradle里设置启动binding
    代码如下：
    android {
        ...
        viewBinding {
        enabled = true
        }
    }
    1.Activity中使用binding
        1.Activity【活动名称-Activity】Binding binding；
        2.bingding.控件id，进行调用
        3.代码演示：
            public ActivityTestBinding binding;
            @Override
            protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            binding= ActivityTestBinding.inflate(getLayoutInflater());
            setContentView(binding.getRoot());
            binding.t1.setText("11");
    2.Fragment中使用binding
        1.代码演示：
        public FragmentNotificationsBinding binding;
        public View onCreateView(@NonNull LayoutInflater inflater,
        ViewGroup container, Bundle savedInstanceState) {
            binding=FragmentNotificationsBinding.inflate(inflater,container,false);
            binding.textNotifications.setText("111");
            return binding.getRoot();
        }

### 服务消息推送

*必须条件：消息渠道、发送消息
    1.消息渠道
        1.`NotificationChannel(String id,CarSequence name, int importance)`
            1.id：String类型，NotificationChannel的id ，每个包必须是唯一的。
            2.name：CharSequence类型，NotificationChannel的用户可见名称
            3.importance：int类型，NotificationChannel的重要性，它控制发送到这个通道的通知中断的方式
        *代码演示
        NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channel);
    2.发送消息
        *代码演示
        //点击通知跳转
        Intent intentMain = new Intent(this, FirstActivity.class);
        //Intent 倾向于立即执行某个动作，而PendIntent倾向于在某个合适的时机再执行某个动作
        //第4个参数用于确定PendingIntent的行为，FLAG_ONE_SHOT\FLAG_NO_CREATE\FLAG_CANCEL_CURRENT\FLAG_UPDATE_CURRENT4
        中值可选，一般默认为0就行
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentMain, 0);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(this,id )
        //android 8.0 以及26以后必须设置channelId,必须和前面对应
        .setContentTitle(title)//标题（必须）
        .setContentText(content)//内容（必须）
        .setSmallIcon(R.mipmap.ic_launcher)//设置小图标  （必须）显示在状态栏上
        .setWhen(System.currentTimeMillis())//指定通知被创建的时间，ms，当下拉系统状态栏时，这里指定的时间会显示在相应通知上，[每次通知都有弹窗]
        .setContentIntent(pendingIntent)//构建一个延迟执行的“意图”，当用户点击这条通知时执行相应逻辑
        .setAutoCancel(false)//取消通知，当为true时，点击通知自动取消
        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))//大图标，下拉时显示
        .setDefaults(NotificationCompat.DEFAULT_ALL)
        //不想进行这么多的设置时使用默认的通知效果，会根据当前手机环境来决定播放什么铃声，以及如何振动
        .build();
        manager.notify(1, notification);//显示通知

### 控件动画

*平移 translate
* 设置方式`View.startAnimation(Animation)`
* TranslateAnimation:平移动画，构造参数：
    1. `TranslateAnimation(Context context, AttributeSet attrs)`
        1. 创建anim文件夹
        2. 在anim文件夹中创建my_anim.xml文件
        3. 文件内容如下
           ```xml
           <?xml version="1.0" encoding="utf-8"?>
           <translate xmlns:android="http://schemas.android.com/apk/res/android"
           android:duration="4500"
           android:fillAfter="true"
           android:fromXDelta="-50%p"
           android:fromYDelta="0"
           android:toXDelta="0"
           android:toYDelta="0" />
           ```
            * `android:duration`动画时长
            * `android:fillAfter`结束后是否停留在最终位置（可不设置）
            * `android:fillBefore`结束后是否停留在开始位置（可不设置）
            * `android:fromXDelta`动画开始时x轴位置
            * `android:fromYDelta`动画开始时y轴位置
            * `android:toXDelta`动画结束时x轴位置
            * `android:toYDelta`动画结束时y轴位置
                * 上述值取值类型
                    1. 数字：比如50.0,相对于本控件50.0px的宽/高度
                    2. 百分比：比如50%,相对于本控件50%的宽/高度
                    3. 百分比+p: 比如上例的-50%p,相对于父控件的50%（此处实际布局为约束布局，默认原点(0,0)在画布中心）
                       -类似与反向
        4. 创建Animation对象
        5. 将对象绑定至需要添加动画的控件中
        6. 代码示例
           Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left);
           left.setAnimation(animation);
		   活动设置，前为启动，后为关闭动画
		   overridePendingTransition(R.anim.null_transition,R.anim.start_activity_out);
           ```
		   
*透明度 alpha
    1.代码示例
    <alpha xmlns:android="http://schemas.android.com/apk/res/android"
    android:fromAlpha="1.0"
    android:toAlpha="0.1"
    android:duration="3000"
    android:fillBefore="true">
    </alpha>
    * `android:fromAlpha`动画开始的透明度
    * `android:toAlpha`动画结束时的透明度
    * 0.0表示全透明，1.0表示完全不透明

*旋转 rotate
    1.代码示例
    <rotate xmlns:android="http://schemas.android.com/apk/res/android"
    android:fromDegrees="0"
    android:toDegrees="-650"
    android:pivotX="50%"
    android:pivotY="50%"
    android:duration="3000"
    android:fillAfter="true">
    </rotate>
    * `android:fromDegrees`动画开始的透明度；正值代表顺时针，负值代表逆时针
    * `android:toDegrees`动画结束时的透明度；正值代表顺时针，负值代表逆时针
    * `android:pivotX`缩放起点X轴坐标
    * `android:pivotY`缩放起点Y轴坐标

*缩放 scale
    1.代码示例
    <scale xmlns:android="http://schemas.android.com/apk/res/android"
		android:fromXScale="0.0"
		android:toXScale="1.4"
		android:fromYScale="0.0"
		android:toYScale="1.4"
		android:pivotX="50"
		android:pivotY="50"
		android:duration="700" />
    * `android:fromXScale`起始的X方向上相对自身的缩放比例，浮点值，比如1.0代表自身无变化，0.5代表起始时缩小一倍，2.0代表放大一倍；
    * `android:toXScale `结尾的X方向上相对自身的缩放比例
    * `android:fromYScale ` 起始的Y方向上相对自身的缩放比例
    * `android:toYScale`结尾的Y方向上相对自身的缩放比例
    * `android:pivotX`缩放起点X轴坐标
    * `android:pivotY`缩放起点Y轴坐标

### 手势监听
        
* 示例代码：
GestureDetector.OnGestureListener test = new GestureDetector.OnGestureListener() {
// 1. 用户轻触触摸屏
@Override
public boolean onDown(MotionEvent e) {
//                System.out.println("!111");
return false;
}
// 2. 用户轻触触摸屏，尚未松开或拖动
// 与onDown()的区别：无松开 / 拖动
// 即：当用户点击的时，onDown（）就会执行，在按下的瞬间没有松开 / 拖动时onShowPress就会执行
@Override
public void onShowPress(MotionEvent e) {

}
// 3. 用户轻击屏幕后抬起
@Override
public boolean onSingleTapUp(MotionEvent e) {
return false;
}
// 4. 用户按下触摸屏 & 拖动
@Override
public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

return true;
}
// 5. 用户长按触摸屏
@Override
public void onLongPress(MotionEvent e) {

}
// 6. 用户按下触摸屏、快速移动后松开
// 参数：
// e1：第1个ACTION_DOWN MotionEvent
// e2：最后一个ACTION_MOVE MotionEvent
// velocityX：X轴上的移动速度，像素/秒
// velocityY：Y轴上的移动速度，像素/秒
@Override
public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
startActivity(new Intent(getApplicationContext(),MainActivity2.class));
return true;
}
};
GestureDetector gestureDetector=new GestureDetector(this,test);
// 步骤2：检测手势：重写View的onTouch函数，将触屏事件交给GestureDetector处理，从而对用户手势作出响应
main.setOnTouchListener(new View.OnTouchListener() {
@Override
public boolean onTouch(View v, MotionEvent event) {
gestureDetector.onTouchEvent(event);

return true; 
}
});

#### 走马灯

#### XML设置背景色等

```xml

<shape xmlns:android="http://schemas.android.com/apk/res/android">
    <!--  圆角  -->
    <corners android:radius="24dp"/>
    <!--  填充  -->
    <solid android:color="@color/white"/>
    <!--  渐变  -->
    <gradient
            android:angle="90"
            android:endColor="@color/black"
            android:centerColor="@color/black"
            android:startColor="#7D7D7D"/>
    <!--  边框  -->
    <stroke
            android:width="2dp"
            android:color="@color/teal_700"/>
    <!--  size和padding忽略  -->
</shape>
```
### 瀑布流

1.    //使用瀑布流   第一个参数为列数  第二个参数为排列方向
        StaggeredGridLayoutManager manager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rv.setLayoutManager(manager);
2. item布局
   <ImageView
   android:layout_width="match_parent"
   android:layout_height="wrap_content"
   android:adjustViewBounds="true"
   android:src="@drawable/t1"
   android:scaleType="fitXY"
   android:id="@+id/iv1"/>
   `android:adjustViewBounds="true"`作用：根据drawable的宽高比例，调制ImageView的设置为WrapContent的那个宽或者高。

### 万能圆角

<androidx.cardview.widget.CardView
android:id="@+id/l2"
android:layout_alignParentRight="true"
android:layout_gravity="center"
android:layout_width="wrap_content"
android:layout_height="wrap_content"
app:cardCornerRadius="12dp">
    <TextView
    android:paddingVertical="2dp"
    android:text="已完成"
    android:textSize="10sp"
    android:paddingHorizontal="10dp"
    android:textColor="#fff"
    android:background="#9CC76C"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"/>
</androidx.cardview.widget.CardView>
    *`Radius=12dp;paddingVertical=2dp;paddingHorizontal=xdp;textSize=10sp`

### 字体大小

    <dimen name="small">10sp</dimen>  eg：位置显示
    <dimen name="textBigPlus">16sp</dimen>
    <dimen name="textSuperBig">18sp</dimen> 大一号
    `TextView默认字体大小15sp`

### 文字设置下划线

1.打开res-values-strings
2.<string name="reg_text"><u>没有账号，点击这里注册</u></string>

### 定时器

*示例代码：
TimerTask timertask=new TimerTask(){
    **定时内容代码
};
Timer timer =new Timer;
timer.schedule(timertask,0,3000);

`timer.schedule(task, delay, period) `
// delay为long,period为long：从现在起过delay毫秒以后，每隔period
`timer.schedule(task, time);   `
// time为Date类型：在指定时间执行一次。

### 排序

1.按时间排序
    1.按时间戳排序or按Date类型时间排序
    List<TestBean> collect = list.stream().sorted(Comparator.comparing(TestBean::getTime)).collect(Collectors.toList());
    *时间戳排序要对时间戳进行除1000，去掉毫秒
    *sorted(xx.reversed()) 降序排序，默认是升序排序
    
### 自定义标题栏
1.去除默认标题栏
    在Manifest中设置对应Activity设置android:theme="@style/Theme.AppCompat.Light.NoActionBar"
2.在xml中写布局
*布局高度设置为：`?android:actionBarSize`

### 设置状态栏颜色
1.在style.xml中新增主题
2.设置主题
    *代码示例：
    setTheme(R.style.AppTheme);
### 设置状态栏颜色

*代码示例
getWindow().setStatusBarColor(0xff4CAF50);

### 打电话

*android 6.0
1.拨号
startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:"+tb.i1)));
2.直接打电话
startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:"+tb.i1)));
*android 6.0以后	 [直接打电话要手动权限]

   if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE)
			!= PackageManager.PERMISSION_GRANTED){
		ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CALL_PHONE},1);
	}else {
		callPhone();
	}

### 调用相机

*代码示例
startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),666)

#### int颜色值

1.16进制写法
t4.setTextColor(0xfffc4907);
    *0x带表16进制，ff代表透明度100% [00代表完全透明，80半透明，cc80%透明]

2.颜色解析
t4.setTextColor(Color.parseColor("#FFEB3B"));

### 下拉选项

*动态绑定数据
private Spinner sp1;
ArrayAdapter s1=new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,prolist);
sp1.setAdapter(s1);
    *`setOnItemSelectedListener()`条目选择监听事件
*静态绑定
1.在res-values下创建arrays
    *代码如下
    <string-array name="app1">
    <item >--精神状态--</item>
    <item >正常</item>
    <item >痴呆</item>
    <item >抑郁</item>
    <item >暴力</item>
    </string-array>
    * `<string-array name="xxx">` 数据名，用于绑定
2.xml绑定
    <Spinner
    android:layout_marginLeft="12dp"
    android:entries="@array/app1"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"/>
    *`android:entries="@array/app1"`数据绑定

### 对话框

*代码示例
AlertDialog.Builder builder=new AlertDialog.Builder(this);
View view=LayoutInflater.from(this).inflate(R.layout.simple_item,null);
AlertDialog alertDialog=builder.create();
alertDialog.setView(view);
alertDialog.show();

### 播放视频

1.本地视频
	*视频放在res下的raw下
private VideoView video;
video.setMediaController(controller);
video.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video01));
video.start();
2.网络视频
String uri2 = "https://flv2.bn.netease.com/videolib1/1811/26/OqJAZ893T/HD/OqJAZ893T-mobile.mp4";
video.setVideoURI(Uri.parse(uri2));
### 保存图片

1.流读写方式保存
    *`onActivityResult(int requestCode, int resultCode, Intent data)`
        1.requestCode 判断是哪个活动返回
        2.resultCode 判断活动返回状态
        3.data 返回数据
    1.重写onActivityResult
    2.创建实体File并设置返回数据
    *`createNewFile()` 创建真实File
    *代码示例
    iv1.setImageURI(data.getData());
    file=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(),"user.jpg");
    try {
    file.createNewFile();
    InputStream read = getContentResolver().openInputStream(data.getData());
    FileOutputStream write = new FileOutputStream(file.getPath());
    int i;
    byte b[]=new byte[8916];
    while ((i=read.read(b))!=-1){
    write.write(b);
    }
    read.close();
    write.close();
    } catch (IOException e) {
    e.printStackTrace();
    }
2.Bitmap保存  
    1.重写onActivityResult
    2.返回数据data转Bitmap
    *代码示例
    bitmap= (Bitmap) data.getExtras().get("data");	
3. uri保存
	binding.iv1.setImageURI(data.getData());
	*有时候data.getData()会返回null，建议bitmap

###  搜索框
<android.support.v7.widget.SearchView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:queryBackground="@null"
    android:background="@drawable/bk"
    app:defaultQueryHint="医院/机构/区域"
    android:id="@+id/ss"
    android:layout_marginHorizontal="12dp"
    app:iconifiedByDefault="false"
    android:layout_marginVertical="12dp"
    app:layout_constraintTop_toBottomOf="@id/sp"
    app:layout_constraintRight_toRightOf="parent"
    app:layout_constraintLeft_toLeftOf="parent"
    />
* `app:queryBackground="@null"`去掉下划线
* `app:iconifiedByDefault="false"`search选中
* `app:defaultQueryHint="医院/机构/区域"`提示内容

#### Switch

* **当`android:showText`为true时才可设置下面两个属性**
* `android:textOff=""`Switch控件未被选中时的文本
* `android:textOn=""`Switch控件被选中时的文本
* `android:thumb="@drawable/thumb"`控件样式
* `android:track="@drawable/track"`轨道样式

### 时间戳和日期互转

    1.时间戳转日期格式
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date=format.format(new Date(System.currentTimeMillis()))；
    2.日期转时间戳
    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String s = String.valueOf(format.parse("2022-08-16 15:40:53").getTime() / 1000);

### 时间显示组件TextClock

### 计时器组件 Chronometer

#### 固定屏幕方向为横向

在AndroidManifest.xml中activity里加上`android:screenOrientation="landscape"`

* portrait:纵向
* unspecified:默认，由系统判断
* sensor:传感器判断

#### drawable中使用Selector选择器

* 子控件为item,item各属性如下
    * `android:color="hex_color"`//颜色值，#RGB,$ARGB,#RRGGBB,#AARRGGBB
    * `android:state_pressed=["true" | "false"]`//是否触摸
    * `android:state_focused=["true" | "false"]`//是否获得焦点
    * `android:state_selected=["true" | "false"]`//是否被状态
    * `android:state_checkable=["true" | "false"]`//是否可选
    * `android:state_checked=["true" | "false"]`//是否选中
    * `android:state_enabled=["true" | "false"]`//是否可用
    * `android:state_window_focused=["true" | "false"]`是否窗口聚焦

#### 改变状态栏主题

* 如果根主题是`Theme.MaterialComponents.DayNight.DarkActionBar`会改变失效
* 需要改为`Theme.AppCompat.Light`

### RadioGroup

* RadioGroup中,RadioButton设为默认选中时，被选中的项需要加id，否则无法取消选择
    ```xml
  <RadioButton
    android:id="@+id/radio1"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:layout_weight="1"
    android:checked="true"
    android:text="select" />
  ```

#### 正则判断手机号码

```
// 正则匹配
Pattern pattern=Pattern.compile("^((13[0-9])|(14[5|7])|(15[0-3|5-9])|(17[013678])|(18[0-4,5-9]))\\d{8}$");
// 进行匹配
Matcher matcher=pattern.matcher("15623625634");
// 匹配结果
matcher.matches()
```
* ^()表示以什么开头
* 以13、145、147、150~153、155~159、170、171、173、176~8、180~4、185~9开头为合法手机号
* \\d匹配数字,{8}重复8次，即手机号码后8位
* $表示结束

#### 正则隐藏身份证号*位

```
idCard.replaceAll("(\\d{2})\\d{12}(\\d{4})","$1****$2")
```

#### TabLayout

* `app:tabIndicatorColor="@android:color/transparent"`设置下划线不显示
* `app:tabSelectedTextColor="@color/green"`选中文本设为绿色

#### recvcleView

* 使用`notifyItemRemoved(position)`造成position混乱时
* 添加`notifyItemRangeChanged(0,list.size)`即可解决

#### bitmap加载图片

* OkHttp(其实都一样):byte[] bytes = response.body().bytes();
  Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
  `imageView.setImageBitmap(bitmap);`最后这步必须主线程运行

#### Json解析

* 单条JSON数据:{"xxx":xxx}
  ```
  try { 
    JSONObject jsonObject = JSONObject(jsonData)
    Object id = jsonObject.get("id")
    Object name = jsonObject.get("name")
  } catch (e: Exception) {
    e.printStackTrace()
  }
  ```
* 多条数据:\[{"xxx":xxx},{"yyy":yyy}\]
  ```
  try {
    val jsonArray = JSONArray(jsonData)
    for (int i = 0; i < info.length(); i++) {
        JSONObject jsonObject = jsonArray.getJSONObject(i)
        Object id = jsonObject.get("id")
        Object name = jsonObject.get("name")
    }
  } catch (e: Exception) {
    e.printStackTrace()
  }
  ```
#### Bean转Json

* 单条JSON数据:{"xxx":xxx}
  ```
  try { 
    JSONObject jsonObject = JSONObject(jsonData)
    jsonObject.put("id",bean.id);
    jsonObject.put("name",bean.name);
  } catch (e: Exception) {
    e.printStackTrace()
  }
  ```
* 多条数据:\[{"xxx":xxx},{"yyy":yyy}\]
  ```
  try {
    JSONArray array = new JSONArray();
    for (int i = 0; i < list.size(); i++) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id",bean.id);
        jsonObject.put("name",bean.name);
        array.put(jsonObject);
    }
  } catch (e: Exception) {
    e.printStackTrace()
  }
  
#### Button

* text带大小写时，默认全为大写
* 需要加上`android:textAllCaps="false"`

#### CheckBox

* 文字显示在右边
* button = "@null"很重要
* drawableEnd:右边，同理可得：start、top、button的效果
  ```xml
  <CheckBox
        android:id="@+id/tip"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:button="@null"
        android:drawableEnd="?android:attr/listChoiceIndicatorMultiple"
        android:text="文字" />
  ```~~